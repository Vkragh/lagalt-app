const getTimeDiff = date => {
	let stringDate = date.toString();
	let convertedDate = new Date(Date.parse(stringDate.split('+')[0]));
	const now = new Date();
	convertedDate.setHours(convertedDate.getHours() + 2);
	const diff = now.getTime() - convertedDate.getTime();
	const days = diff / (1000 * 60 * 60 * 24);
	if (days < 1) {
		let hours = diff / (1000 * 60 * 60);
		if (hours < 1) {
			const minutes = diff / (1000 * 60);
			return `${Math.floor(minutes)} minutes ago`;
		}
		return `${Math.floor(hours)} hours ago`;
	}
	return `${Math.floor(days)} days ago`;
};

export default getTimeDiff;
